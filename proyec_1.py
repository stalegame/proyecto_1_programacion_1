import random
from time import sleep
from os import system


def movimiento(matriz, largo, x, y):

    direccion = random.randint(1, 8)
    while True:
        # blanqueamiento de la particula
        matriz[x][y] = " "

        """El bloque de los siguiente if, tiene la funcion de que la particula
           no sé salga de los limites de la matriz, con las cordenadas x e y.
           esto regido igualmente con la direccion por si se llegara a salir
           por un borde"""

        if x == 0 and direccion < 4:
            break

        if x == largo-1 and direccion > 5:
            break

        if y == 0 and (direccion == 1 or direccion == 4 or direccion == 6):
            break

        if y == largo-1 and (direccion == 3 or direccion == 5
                             or direccion == 8):
            break

        # diagonal izquierda superior
        if direccion == 1:
            x -= 1
            y -= 1
            matriz[x][y] = "*"

        # arriba
        elif direccion == 2:
            x -= 1
            matriz[x][y] = "*"

        # diagonal izquierda superior
        elif direccion == 3:
            x -= 1
            y += 1
            matriz[x][y] = "*"

        # izquierda
        elif direccion == 4:
            y -= 1
            matriz[x][y] = "*"

        # derecha
        elif direccion == 5:
            y += 1
            matriz[x][y] = "*"

        # diagonal izquierda inferior
        elif direccion == 6:
            x += 1
            y -= 1
            matriz[x][y] = "*"

        # abajo
        elif direccion == 7:
            x += 1
            matriz[x][y] = "*"

        # diagonal derecha inferior
        elif direccion == 8:
            x += 1
            y += 1
            matriz[x][y] = "*"

        sleep(0.2)
        system("clear")
        mostrar_matriz(matriz, largo)
    return x, y


def crear_letras(matriz, largo, x, y):

    """ Se emplea la matriz para crear letras y determinar cuales espacios se
        encuentran vacios para no sobreponer letras o la misma particula."""

    letras = {1: "A", 2: "B", 3: "E", 4: "F", 5: "S"}
    contador = 0

    while contador < 9:

        x = random.randint(0, largo-1)
        y = random.randint(0, largo-1)

        if matriz[x][y] == " ":

            valor = random.randint(1, 5)
            matriz[x][y] = letras[valor]
            contador += 1


def particula(matriz, largo):

    """Se crea la particula en una posicion aleatoria y se retorna la
       posicion"""
    x = random.randint(0, largo-1)
    y = random.randint(0, largo-1)
    matriz[x][y] = "*"
    return x, y


def crea_matriz(largo):

    """Se crea la matriz utilizando el tamaño aleatorio y ingresando espacios
       vacios dentro de la misma para que no existan errores de valores
       inexistentes"""
    matriz = []
    for i in range(0, largo):
        temp = []
        for j in range(0, largo):
            temp.append(" ")
        matriz.append(temp)
    return matriz


def mostrar_matriz(matriz, largo):

    """Se utiliza la esta funcion para imprimir la matriz creada"""
    for i in range(largo):
        fila = matriz[i]
        texto = ""
        for item in fila:
            texto = f"{texto} | {item}"
        texto += " |"
        print(texto)


def main():

    tamaño_matriz = random.randint(6, 20)
    matriz = crea_matriz(tamaño_matriz)
    x, y = particula(matriz, tamaño_matriz)
    crear_letras(matriz, tamaño_matriz, x, y)
    mostrar_matriz(matriz, tamaño_matriz)

    while True:
        x, y = movimiento(matriz, tamaño_matriz, x, y)


if __name__ == "__main__":
    main()
